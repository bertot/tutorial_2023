\documentclass[compress]{beamer}
\usepackage[latin1]{inputenc}
\usepackage{hyperref}
\usepackage{alltt}
\usepackage{color}
\setbeamertemplate{footline}[frame number]
\title{Introduction to Coq\\
Part 3: Some libraries}
\author{Yves Bertot}
\date{September 2023}
\mode<presentation>
\begin{document}
\maketitle
\begin{frame}
\frametitle{General recursion}
\begin{itemize}
\item Need to go beyond structural recursion
\item Preserve guarantees of termination, but free from structure constraints
\item In essence, separate the proof of termination from the algorithm description
\end{itemize}
\end{frame}
\begin{frame}
\frametitle{Fueled recursion}
\begin{itemize}
\item The easy trick: count the number of recursive calls
\begin{itemize}
\item Use an extra natural number argument
\item Return a default value upon exhaustion
\end{itemize}
\item Easy to program, but inconvenient
\begin{itemize}
\item Need to figure out how much fuel is enough
\item Any gross over-estimate of fuel slows down the code
\end{itemize}
\item Fuel also clutters the proofs
\begin{itemize}
\item Need to prove that the case of fuel exhaustion is never reached
\item Tantamounts to proving that the intended algorithm was terminating
\end{itemize}
\end{itemize}
\end{frame}
\begin{frame}[fragile]
\frametitle{Example fuel argument}
\begin{verbatim}
Fixpoint fact_fuel (x : Z) (fuel : nat) :=
match fuel with
| 0 => 0
| S p => if x <=? 0 then x * fact_fuel (x - 1) p
end

Definition Zfact (x : Z) := fact_fuel x (S (Z.to_nat x)).
\end{verbatim}
\end{frame}
\begin{frame}
\frametitle{Principled separation of termination proofs}
\begin{itemize}
\item A generic notion of {\em well-founded} relations
\item Show that recursive calls follow such a well-founded relation
\item Proofs can be moved away from algorithmic content
\item Minimal clutter to ensure important tests are remembered
\end{itemize}
\end{frame}
\begin{frame}[fragile]
\frametitle{The Equations plugin}
\begin{verbatim}
From Equations Require Import Equations.
Require Import Wellfounded.

#[local]
Instance zltwf x :
   WellFounded (fun n m => x <= n < m) := (Z.lt_wf x).

Equations Zfact'(x : Z) : Z
     by wf x (fun n m => 0 <= n < m) :=
  Zfact' x with (Z_le_dec x 0) := {
  | left _ => 1
  | right xnle0 => x * Zfact' (x - 1)
  }.
Next Obligation.
lia.
Qed.
\end{verbatim}
\end{frame}
\begin{frame}[fragile]
\frametitle{Comments on Equations}
\begin{itemize}
\item Oriented towards frequent use of dependent types
\item For instance, use of {\tt Z\_le\_dec} of type:\\
{\tt forall x y : Z, \{x <= y\}+\{\verb+~+ x <= y\}}
\item Rely on an inductive with two constructors, where the
  first one contains a proof of \({\tt x} \leq {\tt y}\)
\item This proof must be constructed at definition time
\item The proof is provided at use time and can be used in proofs
\end{itemize}
\end{frame}
\begin{frame}[fragile]
\frametitle{Generic use of boolean test capture}
\begin{verbatim}
Definition inspect {A} (a : A) : {b | a = b} :=
  exist _ a eq_refl.

Notation "x 'eqn:' p" := (exist _ x p)
   (only parsing, at level 20).

Equations Zfact2 (x : Z) : Z
     by wf x (fun n m => 0 <= n < m) :=
  Zfact2 x with inspect (x <=? 0) := {
  | true eqn: xle0 => 1
  | false eqn: xnotle0 => x * Zfact2 (x - 1)
  }.
Next Obligation.
lia.
Qed.
\end{verbatim}
\end{frame}
\begin{frame}
\frametitle{Advantage of the second approach}
\begin{itemize}
\item The boolean algorithm can be programmed as usual
\item Theorems are required to interpret the result
\begin{itemize}
\item In the example {\tt lia} has the knowledge that\\
  {\tt x <=? 0 = false} means {\tt 0 < x}
\end{itemize}
\end{itemize}
\end{frame}
\begin{frame}
\frametitle{Using functions defined by {\tt Equations}}
\begin{itemize}
\item Reliance on proofs makes that computation is rarely possible
\item In proofs: {\tt Equations} provides lemma to be used
for writing
\item In computations: No computation inside Coq, but extraction
makes it possible to generate OCaml code that performs the same
\end{itemize}
\end{frame}
\begin{frame}[fragile]
\frametitle{Example usage in proofs}
\begin{verbatim}
Check (Zfact'_equation_1
     : forall x : Z, Zfact' x =
       Zfact'_unfold_clause_1 x (Z_le_dec x 0)).

Check (Zfact'_unfold_clause_1 =
fun (x : Z) (refine : {x <= 0} + {~ x <= 0}) =>
if refine then 1 else x * Zfact' (x - 1)
     : forall x : Z, {x <= 0} + {~ x <= 0} -> Z).

Lemma Zfact2_main (x : Z) :
  Zfact2 x = if x <=? 0 then 1 else x * Zfact2 (x - 1).
Proof.
rewrite Zfact2_equation_1; simpl.
destruct (x <=? 0); auto.
Qed.

\end{verbatim}
\end{frame}
\begin{frame}[fragile]
\frametitle{Example extraction}
\begin{verbatim}
Extraction Zfact2.
\end{verbatim}
\begin{alltt}\textcolor{blue}{
let rec zfact2 x =
  match inspect (Z.leb x Z0) with
  | True -> Zpos XH
  | False -> Z.mul x (let y = Z.sub x (Zpos XH) in
                      zfact2 y)}
\end{alltt}
\end{frame}
\begin{frame}
\begin{center}
{\Large Real Numbers}
\end{center}
\end{frame}
\begin{frame}
\frametitle{Examples using real numbers}
\begin{itemize}
\item In type theory, only pure lambda-calculus and inductive types have
computation constant
\item Reasoning modulo axioms is possible, but the axioms come without
computation constant
\item Justifying the existence of classical real numbers relies on two
axioms
\item As a result, we can reason about real number computations, but
not perform them in the same way
\end{itemize}
\end{frame}
\begin{frame}[fragile]
\frametitle{Example : computation with the number PI}
\begin{verbatim}
Require Import Reals Lra.

Compute PI.
(* R1 + R1 * (let (x, _) := PI_2_aux in x) *)

Print PI.
(* PI = 2 * PI2 *)

Check PI_2_aux.
(* PI_2_aux : 
   {z | R | 7 / 8 <= z <= 7 / 4 /\ - cos z = 0} *)

Lemma example_formula_with_pi_and_sin : 1 + sin PI = 1.
Proof.
assert (tmp := sin_PI).
lra.
Qed.
\end{verbatim}
\end{frame}
\begin{frame}
\frametitle{How do I compute as with a pocket calculator}
\begin{itemize}
\item Pocket calculator return approximations
\item With minimal guarantees
\begin{itemize}
\item The quality degrades with the number of operations involved
\item Hard to track by users
\item Not satisfactory for proofs
\end{itemize}
\item A proof approach relies on proving equalities or comparisons
\begin{itemize}
\item The previous example was an equality
\item Equalities between real numbers and rational numbers are rare
\item Comparisons are often good enough
\item Even better: intervals
\end{itemize}
\end{itemize}
\end{frame}
\begin{frame}
\begin{center}
\Large Mathematical Components
\end{center}
\end{frame}
\begin{frame}
\frametitle{The Mathematical Components Library}
\begin{itemize}
\item Library initiated by G. Gonthier in the proof of the 4 color theorem
\item Extended for the proof of the odd-order theorem
\item Comes with its own tactic language
\item Contents covering finite types, group theory, finite dimension linear algebra, elementary number theory, ponymials, etc.
\item A principle use of boolean predicates and reflexion
\end{itemize}
\end{frame}
\begin{frame}
\frametitle{A hierarchy of structures}
\begin{itemize}
\item Common theorems should be written (and proved) only once
\item There should be a mechanism to inherit theorems for types that
respect the right structure
\begin{itemize}
\item Type classes
\item Canonical structures
\end{itemize}
\end{itemize}
\end{frame}
\begin{frame}[fragile]
\frametitle{Example canonical structure}
\begin{verbatim}
Require Import Arith ZArith List Bool.

Structure eqtype :=
  { sort : Type;  eq_op : sort -> sort -> bool;
    eq_prop : forall x y, eq_op x y = true <-> x = y}.

Definition count (T : eqtype) (v : sort T):
  list (sort T) -> nat :=
  fold_right
    (fun x r => if eq_op T x v then 1 + r else r) 0.

Fail Check count _ 2 (2 :: 4 :: 5 :: 2 :: nil).

Canonical nat_eqtype := Build_eqtype nat Nat.eqb Nat.eqb_eq.

Check count _ 2 (2 :: 4 :: 5 :: 2 :: nil).
\end{verbatim}
\end{frame}
\begin{frame}[fragile]
\frametitle{Example continued}
\begin{verbatim}
Fail Check count _ 2%Z (2 :: 4 :: 5 :: 2 :: nil)%Z.

Canonical Z_eqtype := Build_eqtype Z Z.eqb Z.eqb_eq.

Check count _ 2%Z (2 :: 4 :: 5 :: 2 :: nil)%Z.
\end{verbatim}
\end{frame}
\begin{frame}
\frametitle{Characteristic of Mathematical Components}
\begin{itemize}
\item Exploit proof irrelevance where it can be proved
\begin{itemize}
\item Types with decidable equality
\item Finite types, etc
\end{itemize}
\item Proposes its own set of tactics
\begin{itemize}
\item Intensive use of rewriting, unfolding
\item Make it easy to exploit changes of point of view
\end{itemize}
\end{itemize}
\end{frame}
\begin{frame}
\frametitle{Example}
\end{frame}
\begin{frame}
\frametitle{Example with matrices}
\begin{itemize}
\item Computing the determinant of a matrix
\end{itemize}
\end{frame}
\begin{frame}
\frametitle{Mathematical idea}
\[
\left(\begin{array}{cccc}
0 & 1 & 0 & 0\\
0 & 0 & 1 & 0\\
0 & 0 & 0 & 1\\
1 & 0 & 0 & 0\\
\end{array}\right)\]
\begin{itemize}
\item The determinant is \((-1)^{n+1}\)
\item  the proof relies on expansion on the first column
\end{itemize}
\end{frame}
\begin{frame}[fragile]
\frametitle{Defining the matrix}
\begin{verbatim}
Definition rotmx n : 'M[K]_n :=
  \matrix_(i < n, j < n) (((i.+1 %% n) == j)%N)%:R.
\end{verbatim}
\begin{itemize}
\item {\tt i} and {\tt j} are bound in the {\tt \backslash{}matrix} notation
\item {\tt i} and {\tt j} are bounded natural numbers
\item Coerced silently into natural numbers for the modulo operation {\tt \%\%}
\item comparison with {\tt j} is at {\tt natural number level}
\item The boolean is silently coerced to 1 or 0
\item Then coerced explicitly into the field {\tt K} using the {\tt \%:R}
notation
\begin{itemize}
\item The latter will be silent in the future
\end{itemize}
\end{itemize}
\end{frame}
\begin{frame}
\frametitle{Demo on a fixed dimension}
\begin{itemize}
\item  computing the determinant for the matrix of size 2
\item Use of {\tt expand_det\_col} giving a natural number to choose the column
\item Use of {\tt mxE} to view a matrix as a function of the two indices
\item Use of {\tt big_ord_recr} to remove elements of the sum one by one
\item Use of theorems for ring structures, inherited by the field {\tt K}
\item Use of {\tt \backaslash=} to cleanup computations and notations
\end{itemize}
\end{frame}
\begin{frame}
\frametitle{Demo on an arbirary dimension}
\begin{itemize}
\item No use of induction
\item After expandin on the first column, use a theorem that distinguishes a
given term of the sum
\item Use of a generic lemma for a big iteration on the neutral element
\item Need to show that all terms are 0
\item use the fact that a bounded integer is smaller than the bound
\item Need to show that the last cofactor is a multiple of the identity matrix
\item Computation on a submatrix, reasoning on index shifts
\end{itemize}
\end{frame}
\end{document}

%%% Local Variables: 
%%% mode: latex
%%% TeX-master: t
%%% End: 
