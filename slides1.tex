\documentclass[compress]{beamer}
\usepackage[latin1]{inputenc}
\usepackage{hyperref}
\usepackage{alltt}
\usepackage{color}
\setbeamertemplate{footline}[frame number]
\title{Introduction to Coq\\
Part 1: the calculus of constructions and inductive types}
\author{Yves Bertot}
\date{September 2023}
\mode<presentation>
\begin{document}
\maketitle
\begin{frame}
\frametitle{A tutorial about Coq}
Objectives of Coq session
\begin{itemize}
\item Write mathematical statements
\item Mark some of these statements as ``proved''
\item Record the proofs for later analysis
\item Perform some guaranteed computations
\end{itemize}
\end{frame}
\begin{frame}
\frametitle{Bare metal and library extensions}
\begin{itemize}
\item User interfaces: jscoq, coq-lsp, vscoq, coqide, emacs
\begin{itemize}
\item Follow download instructions from \url{https://coq.inria.fr}
\item In a hurry, use \url{https://coq.vercel.app/}
\item For a clean sheet, \url{https://coq.vercel.app/scratchpad.html}
\end{itemize}
\item The most basic commands
\begin{itemize}
\item {\tt Check} : just verify that a formula is well formed
\item {\tt Compute} : force the computation
\end{itemize}
\item Working with knowledge that has already been formalized: loading libraries
\begin{itemize}
\item {Loading elementary arithmetic}\quad
{\tt Require Import Arith.}
\item  More advanced arithmetic \qquad\,
{\tt Require Import ZArith.}
\item Some datastructures \qquad\qquad\quad
{\tt Require Import List}
\end{itemize}
\end{itemize}
\end{frame}
\begin{frame}
\frametitle{Bare Coq}
\begin{itemize}
\item Expressions are made of functions applied to arguments
\item Variables receive their value a function application, forever
\begin{itemize}
\item There is no assignment construct that can change the value of a variable
\end{itemize}
\item Anonymous functions can be written by the user for immediate use
\item A point of syntax: parenthesis are not used to represent function 
application
\item Some predefined functions have an infix syntax
\end{itemize}
\end{frame}
\begin{frame}[fragile]
\frametitle{Function usage}
\begin{verbatim}
Check Nat.add 3 5. (* the result shows predefined notation. *)

Check (fun x => 3 + x).  (* temporary use of x *)

Check (fun x => 3 + x) 5. (* at execution x receives 5 *)

Compute (fun x => 3 + x) 5.

Fail Check x. (* x only exists inside the scope of the function *)
\end{verbatim}
Note the syntax to write a function applied to two arguments

Parentheses are not needed to represent function application
\end{frame}
\begin{frame}
\frametitle{Function types}
\begin{itemize}
\item The command {\tt Check} not only verifies that an expression is
correctly written, it also give its type
\item A function with two arguments of type {\tt nat} returning a value
of type {\tt nat} has the following type
\begin{center}
\tt nat -> nat -> nat
\end{center}
\item The arrow {\tt ->} is not associative, but implicit parenthesis are as follows:
\begin{center}
\tt nat -> (nat -> nat)
\end{center}
\end{itemize}
\end{frame}
\begin{frame}
\frametitle{Sorts and Families of types}
\begin{itemize}
\item Some types can a number parameter
\begin{itemize}
\item The type of vectors of a given size
\item The type of numbers under a given bound
\end{itemize}
\item These types are represented by functions whose output is in a type of types
\item Three types of types are given {\tt Set}, {\tt Type}, and {\tt Prop}
\begin{itemize}
\item Types of types are called sort
\end{itemize}
\item For instance {\tt nat} has type {\tt Set}
\item A type of vectors could have type {\tt Type -> nat -> Type}
\item A type of bounded numbers could have type {\tt nat -> Set}
\end{itemize}
\end{frame}
\begin{frame}
\frametitle{Dependent types}
\begin{itemize}
\item Let's assume the existence of a type {\tt vector : Type -> nat -> Type}
\item What would be the type of a function that takes as input a natural
number \(n\) and returns a vector of zeros, of length \(n\)?
\pause
\item {\tt mk0vector : forall n : nat, vector nat n}
\item If the zeros are taken in an existing field type {\tt K}, the type would
be:\\
{\tt mk0vector'  : forall n : nat, vector K n}
\item \(\forall\) is often used instead of {\tt forall}, theoretical lecture also calls this a product type, using \(\Pi\) as notation
\end{itemize}
\end{frame}
\begin{frame}
\frametitle{The logic of dependent types}
\begin{itemize}
\item A universally quantified theorem is a function that yields baby theorems
for every inputs
\item {\bf If} {\tt T1} is the theorem that says that every natural number can be decomposed uniquely into a product of prime numbers, {\bf then} {\tt T1 24} is a theorem that says that 24 can be decomposed \dots
\item In this way, {\tt forall} can really be read as a logical universal quantification
\item This relies on the fact that the theorem statement is understood as a type
\item The sort {\tt Prop} is especially dedicated to types that are used to denote mathematical statements
\end{itemize}
\end{frame}
\begin{frame}[fragile]
\frametitle{Inductive Types}
\begin{itemize}
\item  New types can be defined by providing {\tt constructors} and deducing
a destructor by a minimality argument
\item Running example a set of three elements
\end{itemize}
\begin{verbatim}
Inductive mod3 : Type := Zero | One | Two.

Check Zero.

Definition mod3_to_nat (x : mod3) : nat :=
  match x with Zero => 0 | One => 1 | Two => 2 end.

Definition mod3_succ (x : mod3) : mod3 :=
  match x with Zero => One | One => Two | Two => Zero end.
\end{verbatim}
\begin{itemize}
\item The minimality principle is in the match construct
\item Only required closes are {\tt Zero}, {\tt One}, and {\tt Two}
\end{itemize}
\end{frame}
\begin{frame}[fragile]
\frametitle{Proofs, the bare metal way}
\begin{verbatim}
Definition le_2_2 : 2 <= 2 := le_n 2.

Definition le_1_2 : 1 <= 2 := le_S 1 1 (le_n 1).

Definition le_0_2 : 0 <= 2 := le_S 0 1 (le_S 0 0 (le_n 0)).

Definition mod3_to_nat_le_2 (x : mod3) :
  mod3_to_nat x <= 2 :=
match x with
| Zero => le_0_2
| One => le_1_2
| Two => le_2_2
end.
\end{verbatim}
\end{frame}
\begin{frame}[fragile]
\frametitle{The elimination principle, for proofs}
\begin{itemize}
\item Proving that a property holds for all elements of an inductive type
\item One {\bf only} needs to check that property for every constructor
\begin{itemize}
\item The minimality principle that I mentioned before
\end{itemize}
\end{itemize}
\begin{verbatim}
Definition mod3_cases (P : mod3 -> Prop) (x : mod3)
  (h0 : P Zero) (h1 : P One) (h2 : P Two) : P x :=
  match x with
  | Zero => h0
  | One => h1
  | Two => h2
  end.
\end{verbatim}
\end{frame}
\begin{frame}[fragile]
\frametitle{Inductive types with recursion}
\begin{itemize}
\item Each constructor may be a function
\item Arguments of the function may belong to the type being defined
\end{itemize}
\begin{verbatim}
Inductive list (A : Type) : Type :=
| nil : list A
| cons : A -> list A -> list A.

Check cons nat 3 (cons nat 2 (cons nat 1 (nil nat))).
\end{verbatim}
\end{frame}
\begin{frame}
\frametitle{structural recursion}
\begin{itemize}
\item elements of inductive types with recursion can contain arbitrary large
amounts of information
\item Recursive programming can handle all this data in computations
\item The command to define a recursive function is called {\tt Fixpoint}
\item Restricted recursion by comparison with conventional functional programming
\item Guaranteed termination achieved through a syntactic criterion
\item Recursive calls only allowed on subterms obtained by pattern-matching
\item The generic reasoning principle (akin to {\tt mod3\_cases}) is an induction principle (with induction hypothesis)
\end{itemize}
\end{frame}
\begin{frame}[fragile]
\frametitle{Example recursive programming with lists}
\begin{verbatim}
Fixpoint fold_right (A B : Type)
  (f : A -> B -> B)(v : B)(l : list A) : B :=
match l with
| nil _ => v
| cons _ x tl => f x (fold_right A B f v tl)
end.

Compute fold_right nat nat Nat.add 0 
  (cons nat 3 (cons nat 2 (cons nat 1 (nil nat)))).
\end{verbatim}
\end{frame}
\begin{frame}
\frametitle{Matters of productivity and efficiency}
\begin{itemize}
\item The predefined package of lists is more practical to use than the type
shown in these slides
\item Notations and implicit arguments make it possible to avoid writing obvious arguments
\item Lists are linear representations of data collections, with an access
cost that is linear with respect to the amount of stored data
\begin{itemize}
\item conventional programming languages like OCaml provide quasi constant access
\item Other data-structures, like binary search trees or tries, provide much faster access
\end{itemize}
\item Numbers have the same variability in efficiency
\begin{itemize}
\item Binary structures are used to represent integers
\item Addition, multiplication, division are natural to program structurally
\item Other functions require inventiveness
\end{itemize}
\end{itemize}
\end{frame}
\begin{frame}[fragile]
\frametitle{For the record: factorial function with binary numbers}
\begin{verbatim}
Require Import ZArith.

Fixpoint fact' (p : positive) (offset : Z) : Z :=
match p with
| xH => (offset + 1)%Z
| xO p => fact' p offset * (fact' p (offset + (Zpos p)))
| xI p => (2 * (Zpos p) + 1 + offset) *
          fact' p offset * fact' p (offset + (Zpos p))
end.

Definition Zfact (x : Z) : Z :=
  match x with | Zpos p => fact' p 0 | _ => 1%Z end.

Compute Zfact 50.
\end{verbatim}
Computing large factorials will fail in the web-browser, but other instances of Coq will have no problems
\end{frame}
\begin{frame}[fragile]
\frametitle{dependent families of inductive types}
\begin{itemize}
\item The type {\tt list} already presented is actually a family of inductive types
\item The parameter may be a piece of data, and the type may be empty or inhabited depending on the parameter
\item The simplest example: identity
\end{itemize}
\begin{verbatim}
Inductive eq (A : Type) (x : A) : A -> Prop :=
  eq_refl : eq A x x.
\end{verbatim}
\begin{itemize}
\item This is how equality is represented in Coq
\item The generic reasoning principle (like {\tt mod3} above) has an important
meaning
\begin{itemize}
\item if {\tt eq A x y} holds, then every property that holds for {\tt x} also holds for {\tt y}
\end{itemize}
\end{itemize}
\end{frame}
\begin{frame}
\frametitle{Pervasive use of inductive families of types for logic}
\begin{itemize}
\item Logical connectives such as conjunction, disjunction, Truth, and Falsehood
are described as inductive types or type families
\item Existential quantification also
\item Equality also
\item constructors give introduction rules, reasoning principles (based on pattern-matching) give elimination rules
\item In proofs, this will be made apparent by the use of a single proof command for several behaviors
\end{itemize}
\end{frame}
\begin{frame}
\frametitle{Existing data structures}
\begin{itemize}
\item Natural numbers, type {\tt nat}, interpretation by default of arithmetic notations, more functions available after:\\
{\tt Require Import Arith.}
\item integers, type {\tt Z}, based on a binary encoding, available after:\\
{\tt Require Import ZArith.}\\
arithmetic notations
can aim to this type after a simple command
\item rational numbers, type {\tt Q}, available after:\\
{\tt Require Import QArith.}
\item Lists, type {\tt list}, available after:\\
{\tt Require Import List.}
\item Various forms of binary trees, with efficient adding and lookup functions
\item Computation can be performed for recursive functions on these datatypes,
using the {\tt Compute} command.
\end{itemize}
\end{frame}
\begin{frame}
\frametitle{Proofs from the practical side}
\begin{itemize}
\item Logical statements are types
\item When there is an element in the type, the statement is proved
\item Making proofs is constructing objects in types
\item This can be done by writing programs (as was shown already)
\item This is impratical for proofs of reasonable statements
\begin{itemize}
\item It is practical in Agda, but the user-interface has been fine-tuned for that
\end{itemize}
\item In Coq, one resorts to a proof mode, goals, and tactics
\end{itemize}
\end{frame}
\begin{frame}[fragile]
\frametitle{Demo: computing 10 digits of PI}
\begin{small}
\begin{verbatim}
Require Import Arith QArith.

Coercion Z.of_nat : nat >-> Z.

Fixpoint atan_approx (n : nat) (x : Q) :=
  match n with
  | 0%nat => x
  | S p => (-1) ^ S p / (2 * S p + 1 # 1) * 
           x ^ (2 * S p + 1) + atan_approx p x
  end.

Definition pi_digits (n m : nat) :=
  let v := 4 * (atan_approx n (1/2) + atan_approx n (1/3)) in
  (Qnum v * 10 ^ m / Zpos (Qden v))%Z.

Time Compute pi_digits 15 10.
(* result in less than 0.02 secs on my machine *)
\end{verbatim}
\end{small}
\end{frame}
\begin{frame}[fragile]
\frametitle{Proof mode}
\begin{itemize}
\item Entering the mode
\end{itemize}
\begin{verbatim}
Lemma example0 : forall (A : Prop) A -> A.
Proof.
\end{verbatim}
\begin{alltt}\textcolor{blue}{
  ============
  forall A : Prop, A -> A
}\end{alltt}
\begin{itemize}
\item The current goal is the statement we want to prove
\item The next three commands called tactics will modify the goal
\end{itemize}
\end{frame}
\begin{frame}[fragile]
\frametitle{Transforming the goals}
\begin{verbatim}
Lemma example0 : forall (A : Prop) A -> A.
Proof.
intros A hyp_A.
\end{verbatim}
\begin{alltt}\textcolor{blue}{
  A : Prop
  hyp_A : A
  ============
  A
}\end{alltt}
\begin{itemize}
\item The top of the bar is a {\em context}
\begin{itemize}
\item It contains things that are assumed to exist
\item For instance, {\tt hyp\_A : A} means: ``{\tt hyp\_a} is a proof of {\tt A}''
\end{itemize}
\item The text below the bar is what we need to prove
\end{itemize}
\end{frame}
\begin{frame}[fragile]
\frametitle{Transforming the goals (2)}
\begin{verbatim}
Lemma example0 : forall (A : Prop) A -> A.
Proof.
intros A hyp_A.
exact hyp_A.
\end{verbatim}
\begin{alltt}\textcolor{blue}{
No more goals
}\end{alltt}
\begin{itemize}
\item When a solution is found for a goal, it disappears
\item If there were several goals, the system displays the next one
\item For beginners, this can be puzzling
\begin{itemize}
\item the new goal may look that a transformation of the previous one,
even though they are rather unrelated
\end{itemize}
\end{itemize}
\end{frame}
\begin{frame}[fragile]
\frametitle{Finishing a proof}
\begin{verbatim}
Qed.
\end{verbatim}
\begin{itemize}
\item You have to type {\tt Qed.} at the end of a proof
\item Othewise
\begin{itemize}
\item The theorem is not saved
\item You do not exit proof mode
\item You cannot start another proof
\end{itemize}
\item Other ways to exit proof mode
\begin{itemize}
\item {\tt Admitted.}  The theorem is saved, but recorded as not actually proved
\item {\tt Abort.} The theorem is not saved
\item {\tt Defined.}  Like {\tt Qed.}, but different on a technicality
\end{itemize}
\end{itemize}
\end{frame}
\begin{frame}
\frametitle{A large number of tactics}
\begin{itemize}
\item Step tactics for basic logical connectives: {\tt intros}, {\tt assert}, 
{\tt apply},
{\tt exact}, {\tt destruct}, {\tt split}, {\tt left}, {\tt right}, {\tt exists}
\item Tactics for equality reasoning: {\tt reflexivity}, {\tt rewrite}, {\tt replace}
\item Tactics for defined functions: {\tt unfold}, {\tt fold}, {\tt change}
\item Specialized tactics for inductive types: {\tt induction}, {\tt case}, {\tt discriminate}, {\tt injection}, {\tt simpl}, {\tt cbv}
\item Automation tactics: {\tt auto}, {\tt tauto}, {\tt intuition}
\item Domain specific automated tactics: {\tt ring}, {\tt lia}, {\tt lra}, {\tt nia}, {\tt nra}, {\tt interval} (only loaded upon request)
\end{itemize}
\end{frame}
\begin{frame}
\frametitle{A beginner's tactic table}
\begin{scriptsize}
\begin{tabular}{|c|c|c|c|}
\hline
&\(\Rightarrow\)&\(\forall\)&\(\wedge\)\\
\hline
Hypothesis {\tt H}&{\tt apply H}&{\tt apply H}&{\tt destruct H as [H1 H2]}\\
\hline
conclusion&{\tt intros {\tt H}} &{\tt intros {\tt H}} &{\tt split}\\
\hline
\hline
&\(\neg\)&\(\exists\)&\(\vee\)\\
\hline
Hypothesis {\tt H}&
{\tt destruct H}&{\tt destruct H as [x H1]}&{\tt destruct H as [H1 | H2]}\\
\hline
conclusion&{\tt intros H}&{\tt exists} {\it v}&{\tt left}
or\\
&&&{\tt right}\\
\hline
\hline
&\(=\)&{\tt False}&\\
\hline
Hypothesis {\tt H}&{\tt rewrite H}&{\tt destruct H}&\\
&{\tt rewrite <- H}&&\\
\hline
conclusion&{\tt reflexivity}&&\\
&{\tt ring}&&\\
\hline
\end{tabular}
\end{scriptsize}
\end{frame}
\begin{frame}[fragile]
\frametitle{Goal handling tactics}
\begin{itemize}
\item {\tt exact} will solve a goal by providing an assumption from the context
that is the same
\item {\tt assert (hyp\_name : {\sl statement})} will create two goals
\begin{itemize}
\item In the first you have to prove {\sl statement}
\item In the secon you have an extra hypothesis {\tt hyp\_name} stating
that {\sl statement} holds 
\end{itemize}
\item Very useful to state intermediary steps in your proof, to make it more
readable
\end{itemize}
\end{frame}
\begin{frame}
\frametitle{Proofs by induction}
\begin{itemize}
\item {\tt induction {\sl e}} will be available anytime {\sl e} belongs to an
inductive type
\item The proof will follow a canonical structure, requiring to check all
constructors of the inductive type, providing induction hypotheses when
relevant
\end{itemize}
\end{frame}
\begin{frame}
\frametitle{Demo time}
\end{frame}
\begin{frame}
\frametitle{Real numbers}
\begin{itemize}
\item Real numbers cannot be described by inductive type
\item We cannot use the {\tt Compute} command to obtain a ``better form''
 of a real number
\item However, we can compute in proof
\begin{itemize}
\item We can verify that two real numbers are equal
\item We can add an hypothesis that states an approximation of value
\end{itemize}
\end{itemize}
\end{frame}
\end{document}

%%% Local Variables: 
%%% mode: latex
%%% TeX-master: t
%%% End: 
