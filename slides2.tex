\documentclass[compress]{beamer}
\usepackage[latin1]{inputenc}
\usepackage{hyperref}
\usepackage{alltt}
\usepackage{color}
\setbeamertemplate{footline}[frame number]
\title{Introduction to Coq\\
Part 2: Automation tactics}
\author{Yves Bertot}
\date{September 2023}
\mode<presentation>
\begin{document}
\maketitle
\begin{frame}
\frametitle{Automated tactics}
\begin{itemize}
\item Logical workhorses
\item Polynomial equalities
\item Linear arithmetic
\item Interval reasoning
\end{itemize}
\end{frame}
\begin{frame}
\frametitle{Simple logic reasoning with databases of theorems}
\begin{itemize}
\item The tactic {\tt auto} applies theorems from a given database
\item Applicability is tested through pattern-matching
\item Only theorems where unknowns in premise also appear in the conclusion
\item Users can create their own databases of theorems
\item Hypotheses of the current goal are automatically included
\item The depth of proof search is limited to 5 by default
\begin{itemize}
\item writing {\tt auto \(n\)} uses \(n\) instead of 5
\item Information packed in conjunction is not used (look at {\tt firstorder})
\item Some Hints can be fine-tuned to force a more advanced behavior
\end{itemize}
\end{itemize}
\end{frame}
\begin{frame}[fragile]
\frametitle{Example with {\tt auto} and the local context}
\begin{verbatim}
Lemma use_hyp_in_auto :
  forall P : nat -> Prop,
  (forall x y, P x -> P y -> P (x + y)) ->
  forall a b c d, P a -> P b -> P c -> P d ->
  P (a + ((b + c)) + d).
Proof.
auto.
Qed.
\end{verbatim}
\end{frame}
\begin{frame}[fragile]
\frametitle{Adding theorems to a database}
\begin{verbatim}
#[export]
Hint Resolve Nat.Even_Even_add : ev_base.
#[export]
Hint Resolve Nat.Even_mul_r : ev_base.
#[export]
Hint Resolve Nat.Even_mul_l : ev_base.

Lemma Even2 : Nat.Even 2.
Proof. exists 1. reflexivity. Qed.

#[export]
Hint Resolve Even2 : ev_base.
\end{verbatim}
If you only want to improve the default database, its name is {\tt core}
\end{frame}
\begin{frame}[fragile]
\frametitle{Using a database}
\begin{verbatim}
Lemma use_thms_in_auto n m :
  Nat.Even ((((2 * n + m * 2) + (2 * n + m * 2)) + 
            ((2 * n + m * 2) + (2 * n + m * 2))) +
            (((2 * n + m * 2) + (2 * n + m * 2)) + 
            ((2 * n + m * 2) + (2 * n + m * 2)))).
Proof.
auto 20 with ev_base.
Qed.
\end{verbatim}
\end{frame}
\begin{frame}
\frametitle{Intuitionistic tautologies}
\begin{itemize}
\item Some automatic tools expand all conjunctions and disjunctions in the
context before calling {\tt auto}
\item intuition, tauto, firstorder
\end{itemize}
\end{frame}
\begin{frame}
\frametitle{Reasoning about numerical equalities}
\begin{itemize}
\item Some automatic tactic solves all equality proofs about addition,
multiplication, neutral elements, and subtraction (when possible)
\item When it fails, three things may occur
\begin{itemize}
\item The equality you want to prove is simply not true
\item You have more knowledge than what the {\tt ring} tactic has
access to
\item You are actually in the {\tt nat} semi-ring, and your formula contains
subtractions
\end{itemize}
\item This tactic is also extensible: you can adapt it to your pet ring
\end{itemize}
\end{frame}
\begin{frame}[fragile]
\frametitle{ring example}
\begin{verbatim}
Require Import ZArith.
Open Scope Z_scope.

Lemma quartic_sub a b :
  (a - b) ^ 4 =
  a ^ 4 - 4 * a * b ^ 3 + 6 * a ^ 2 * b ^ 2 -
  4 * a ^ 3 * b + b ^ 4.
Proof.
ring.
Qed.
\end{verbatim}
\end{frame}
\begin{frame}[fragile]
\frametitle{An example failure in {\tt nat} }
\begin{verbatim}
Lemma quartic_sub_nat (a b : nat) :
  (a - b) ^ 4 =
  a ^ 4 - 4 * a * b ^ 3 + 6 * a ^ 2 * b ^ 2 - 4 * a ^ 3 * b + b ^ 4.
Proof.
Fail ring.
cbv [Nat.pow].
Fail ring.
\end{verbatim}
\textcolor{blue}{Thus statement is simply not true}
\begin{verbatim}
Abort.
\end{verbatim}
\end{frame}
\begin{frame}
\frametitle{More structure: automatic equality proofs in fields}
\begin{itemize}
\item There is also a {\tt field} tactic
\item It can also handle division, but the divisors have to be proved non-zero
\item It tries to prove non-zero conditions directly
\item May produce extra goals
\end{itemize}
\end{frame}
\begin{frame}[fragile]
\frametitle{Example use of {\tt field}}
\begin{verbatim}
Require Import Reals Lra.

Open Scope R_scope.

Lemma sum_step n : n * (n + 1) / 2 + (n + 1) =
    (n + 1) * (n + 2) / 2.
Proof.
field.
Qed.

Lemma gen_sum_step n f : f <> 0 ->
  n * (n + 1) / f + (n + 1) = 
  (n + 1) * (n + f) / f.
Proof.
intros fn0.
field.
auto.
Qed.
\end{verbatim}
\end{frame}
\begin{frame}
\frametitle{Numeric comparison: linear arithmetic}
\begin{itemize}
\item You have to request for them, but there are tactics to reason about
comparisons between linear formulas
\item The tactics are named {\tt lia} (for integers) or {\tt lra} for
real numbers
\end{itemize}
\end{frame}
\begin{frame}[fragile]
\frametitle{Example proof with {\tt lra}}
\begin{verbatim}
Lemma example_linear_arithmetic x y :
  (x * x) / 2 + 1 <= y ->
  y <= - 2 * (x * (x * 1)) + 1 ->
  y <= (x * x) + 10 ->
  (x * x) <= y.
Proof.
intros half_plane_1 half_plane_2 half_plane_3.
lra.
Qed.
\end{verbatim}
\end{frame}
\begin{frame}
\frametitle{linear arithmetics only knows integer constants}
\begin{itemize}
\item Mathematical constants like \(e\) or \(\pi\) are treated as variables
\item For use with {\tt lra}, the {\tt interval} tactic can be used
\end{itemize}
\end{frame}
\begin{frame}[fragile]
\frametitle{Example using {\tt interval}}
\begin{verbatim}
From Interval Require Import Tactic.

Lemma example_with_PI x : PI <= x -> x < 4 * x.
Proof.
intros xgepi.
Fail lra.
interval_intro PI.
lra.
Qed.
\end{verbatim}
\end{frame}
\begin{frame}[fragile]
\frametitle{Interval answers interval questions with rational bounds}
In my experiments, it refuses questions with strict comparisons
\begin{verbatim}
Lemma approx_pi : 3.14 <= PI <= 3.15.
Proof.
interval.
Qed.
\end{verbatim}
\end{frame}
\end{document}

%%% Local Variables: 
%%% mode: latex
%%% TeX-master: t
%%% End: 
